# ZFS Tools

This repo includes scripts to help with the creation and management of ZFS pools.

## Disk Labeling

The first script, `label_disks.sh`, formats specified disks and applies a GPT label with relevant data, such as location, size, and serial number.

## Ubuntu Installer

The script `install_ubuntu_on_zfs.sh` walks through the steps to install a base Ubuntu system (22.04 by default) using ZFS as the root file system. The script is configured for EFI systems and is based on information from OpenZFS.org (https://openzfs.github.io/openzfs-docs/).