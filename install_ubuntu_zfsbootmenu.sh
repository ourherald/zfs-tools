#!/usr/bin/env bash

# Install Ubuntu with zfsbootmenu

echo "Welcome to the Ubuntu ZFS installer"
echo "These disks are available on your system:"
lsblk

read -p "Please choose a disk on which to install Ubuntu: " diskname

read -s -p "Please enter a passphrase with which to encrypt the zpool: " SomeKeyphrase

version_code=jammy

# Get environment variables from os-release of live system
source /etc/os-release
export ID

# Install helpers
apt update
apt install debootstrap gdisk zfsutils-linux

# Generate /etc/hostid file
zgenhostid -f $(hostid)

export BOOT_DISK="/dev/${diskname}" # Change this to match local environment
export BOOT_PART="1"
export BOOT_DEVICE="${BOOT_DISK}p${BOOT_PART}"

export POOL_DISK="/dev/${diskname}"
export POOL_PART="2"
export POOL_DEVICE="${POOL_DISK}p${POOL_PART}"

# Clear disks
zpool labelclear -f "$POOL_DISK"

wipefs -a "$POOL_DISK"
wipefs -a "$BOOT_DISK"

sgdisk --zap-all "$POOL_DISK"
sgdisk --zap-all "$BOOT_DISK"

# Create EFI partition
sgdisk -n "${BOOT_PART}:1m:+512m" -t "${BOOT_PART}:ef00" "$BOOT_DISK"

# Create zpool partition
sgdisk -n "${POOL_PART}:0:-10m" -t "${POOL_PART}:bf00" "$POOL_DISK"

# Setup encryption
echo "$SomeKeyphrase" > /etc/zfs/zroot.key
chmod 000 /etc/zfs/zroot.key

# Create ZPOOL
zpool create -f -o ashift=12 \
 -O compression=lz4 \
 -O acltype=posixacl \
 -O xattr=sa \
 -O relatime=on \
 -O encryption=aes-256-gcm \
 -O keylocation=file:///etc/zfs/zroot.key \
 -O keyformat=passphrase \
 -o autotrim=on \
 -o compatibility=openzfs-2.1-linux \
 -m none zroot "$POOL_DEVICE"
 
 # Create initial file system
zfs create -o mountpoint=none zroot/ROOT
zfs create -o mountpoint=/ -o canmount=noauto zroot/ROOT/${ID}
zfs create -o mountpoint=/home zroot/home

zpool set bootfs=zroot/ROOT/${ID} zroot

# Get ZPOOL ready for installation
zpool export zroot
zpool import -N -R /mnt zroot
zfs load-key -L prompt zroot

zfs mount zroot/ROOT/${ID}
zfs mount zroot/home

if [[ $(mount | grep mnt | grep -c zroot) -lt 1 ]]; then
	echo "It looks like zroot isn't mounted at /mnt; something must have gone wrong."
	exit 1
fi

# Update device symlinks
udevadm trigger

# Install Ubuntu
debootstrap $version_code /mnt

# BASIC SETUP

# Copy files into the new installation
cp /etc/hostid /mnt/etc/hostid
cp /etc/resolv.conf /mnt/etc/
mkdir /mnt/etc/zfs
cp /etc/zfs/zroot.key /mnt/etc/zfs


cat << EEOF >> /mnt/opt/zfs_ubuntu_setup.sh
# Set a hostname
echo "$new_hostname" > /etc/hostname
echo -e "127.0.1.1\t${new_hostname}" >> /etc/hosts

# Setup root password
echo "Set up your root password: "
passwd

cat <<EOF > /etc/apt/sources.list
# Uncomment the deb-src entries if you need source packages

deb http://archive.ubuntu.com/ubuntu/ $version_code main restricted universe multiverse
# deb-src http://archive.ubuntu.com/ubuntu/ $version_code main restricted universe multiverse

deb http://archive.ubuntu.com/ubuntu/ $version_code-updates main restricted universe multiverse
# deb-src http://archive.ubuntu.com/ubuntu/ $version_code-updates main restricted universe multiverse

deb http://archive.ubuntu.com/ubuntu/ $version_code-security main restricted universe multiverse
# deb-src http://archive.ubuntu.com/ubuntu/ $version_code-security main restricted universe multiverse

deb http://archive.ubuntu.com/ubuntu/ $version_code-backports main restricted universe multiverse
# deb-src http://archive.ubuntu.com/ubuntu/ $version_code-backports main restricted universe multiverse

deb http://archive.canonical.com/ubuntu/ $version_code partner
# deb-src http://archive.canonical.com/ubuntu/ $version_code partner
EOF

# Run updates
apt update
apt upgrade -y

# Install basic packages
apt install --no-install-recommends -y linux-generic locales keyboard-configuration console-setup 

# Install custom packages
apt install -y tmux vim curl git wpasupplicant

# Install ZFS tools
apt install dosfstools zfs-initramfs zfsutils-linux

# Enable zfs
systemctl enable zfs.target
systemctl enable zfs-import-cache
systemctl enable zfs-mount
systemctl enable zfs-import.target

# Configure initramfs
echo "UMASK=0077" > /etc/initramfs-tools/conf.d/umask.conf
update-initramfs -c -k all

# zfsbootmenu
zfs set org.zfsbootmenu:commandline="quiet" zroot/ROOT
zfs set org.zfsbootmenu:keysource="zroot/ROOT/${ID}" zroot

mkfs.vfat -F32 "$BOOT_DEVICE"

cat << EOF >> /etc/fstab
$( blkid | grep "$BOOT_DEVICE" | cut -d ' ' -f 2 ) /boot/efi vfat defaults 0 0
EOF

mkdir -p /boot/efi
mount /boot/efi

mkdir -p /boot/efi/EFI/ZBM
curl -o /boot/efi/EFI/ZBM/VMLINUZ.EFI -L https://get.zfsbootmenu.org/efi
cp /boot/efi/EFI/ZBM/VMLINUZ.EFI /boot/efi/EFI/ZBM/VMLINUZ-BACKUP.EFI

# Configure EFI boot entries
mount -t efivarfs efivarfs /sys/firmware/efi/efivars
apt install efibootmgr
efibootmgr -c -d "$BOOT_DISK" -p "$BOOT_PART" \
  -L "ZFSBootMenu (Backup)" \
  -l '\EFI\ZBM\VMLINUZ-BACKUP.EFI'

efibootmgr -c -d "$BOOT_DISK" -p "$BOOT_PART" \
  -L "ZFSBootMenu" \
  -l '\EFI\ZBM\VMLINUZ.EFI'
  
# Exit chroot
echo "We're done with the chroot unless there's more you'd like to do. Type \"exit\" and once you're back at the live boot command prompt, run \"unmount -n -R /mnt\" and then \"zpool export zroot\" before rebooting."

EEOF

echo "Done with initial setup."
echo "The rest of the installation will take place in a chroot."
echo "Once you're in the chroot, run the script in /opt/zfs_ubuntu_setup.sh"
echo "After it's all over and you're out of the chroot, export zroot and reboot."
echo "Here we go!"

# chroot into new OS
echo "Mounting supporting filesystems ..."
mount -t proc proc /mnt/proc
mount -t sysfs sys /mnt/sys
mount -B /dev /mnt/dev
mount -t devpts pts /mnt/dev/pts
echo "Switching to chroot ..."
chroot /mnt $(which bash)


## Finish things up
#zpool export zroot
#reboot
