#!/usr/bin/env bash
#
# Script to format, partition, and create labels for disks to be used in ZFS pools
# Label includes physical location of disk for ease of replacement.



size () {
    local -a units
    local -i scale
    if [[ "$1" == "-si" ]]
    then
        scale=1024
        units=(B KiB MiB GiB TiB EiB PiB YiB ZiB)
        shift
    else
        scale=1000
        units=(B K M G T E P Y Z)
    fi
    local -i unit=0
    if [ -z "${units[0]}" ]
    then
        unit=1
    fi
    local -i whole=${1:-0}
    local -i remainder=0
    while (( whole >= $scale ))
    do
        remainder=$(( whole % scale ))
        whole=$((whole / scale))
        unit=$(( $unit + 1 ))
    done
    local decimal
    if [ $remainder -gt 0 ]
    then
        local -i fraction="$(( (remainder * 10 / scale)))"
        if [ "$fraction" -gt 0 ]
        then
            decimal=".$fraction"
        fi
    fi
    var=${whole}${decimal}
    final=$(echo $var | awk '{print int($1+0.5)}')
    echo $final${units[$unit]}
}

func_info() {
    echo "--> Entering info mode."
    echo " "
    read -p "Enter the names of the disks for which you'd like information in a space-separated list (e.g. da3 da4). Enter them in their index order. " -r disks

    # Enter the names of disks as the `disks` array.

    devices=("$disks")

    for disk in ${devices[@]}; do
        sn=$(udevadm info --query=all --name=$disk | grep ID_SCSI_SERIAL | awk -F'=' '/ID_SCSI_SERIAL=/ {print $2}')
        capacity=$(size $(lsblk -b /dev/$disk | grep -w "$disk" | awk '{print $4}'))
	    echo "$disk ... ${capacity} ... ${sn}"
    done
}

func_help() {
    echo "This script formats geom devices for use with zfs pools and labels"
    echo "and labels the active partition with a useful name. Here are its options:"
    echo "    -h    Show this help menu"
    echo "    -s    Single-disk mode (specify a single disk)"
    echo "    -m    Multiple-disk mode (specify an arrays of disks)"
    echo "    -i    Get info mode (specify an array of disks)"
}

func_format() {
    # Test for privileges
    if [[ $UID == 0 ]]; then
        gpart_cmd=/sbin/gpart
        sgdisk_cmd=/usr/sbin/sgdisk
    else
        gpart_cmd='/usr/local/bin/sudo /sbin/gpart'
        sgdisk_cmd='/usr/bin/sudo /usr/sbin/sgdisk'
    fi

    if [[ $(uname) == FreeBSD ]]; then
        "$gpart_cmd" destroy -F $disk
        "$gpart_cmd" create -s GPT $disk
        "$gpart_cmd" add -t freebsd-swap -a 1m -s 2G $disk
        "$gpart_cmd" add -t freebsd-zfs -a 1m -l "${name}" $disk
    elif [[ $(uname) == Linux ]]; then
        "$sgdisk_cmd" -Z $disk
        "$sgdisk_cmd" -a1 -n1:34:2047 -t1:8200 -n2:2048:0 -t2:a504 -c2:${name} /dev/$disk
    fi
}

func_variables() {
    if [[ $(uname) == FreeBSD ]]; then
        sn=$(geom disk list $disk | awk -F ': ' '/ident:/ {print $2}')
        capacity=$(size $(geom disk list $disk | awk -F ': ' '/Mediasize:/ {print $2}' | awk '{print $1}'))
    elif [[ $(uname) == Linux ]]; then
        sn=$(udevadm info --query=all --name=$disk | grep ID_SCSI_SERIAL | awk -F'=' '/ID_SCSI_SERIAL=/ {print $2}')
        capacity=$(size $(lsblk -b /dev/$disk | grep -w "$disk" | awk '{print $4}'))
    fi
}

func_multi() {
    echo "--> Entering multiple-disk mode."
    echo " "
    read -p "Enter the names of the disks you'd like to format in a space-separated list (e.g. da3 da4). Enter them in their index order. " -r disks

    # This is the batch version of this script.
    # Enter the names of disks as the `disks` array.

    devices=("$disks")
    read -p "Choose a prefix to start off the partition label; an index value will be added. (Specifying the location is a good option---s0d, e.g.) " -r prefix
    read -p "What is the first index number of the disks? " -r bay

    idx="${bay}"
    for disk in ${devices[@]}; do
        func_variables
        name=${prefix}${idx}-${capacity}-${sn}
	    echo "$disk ... ${name}"
    	((idx++))
    done
	echo " "
    read -p "If you confirm, the above disks will be erased, partitioned, and labeled as shown. Should we continue? [y|n] " -n 1 -r
    echo " "
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        idx="${bay}"
        for disk in ${devices[@]}; do
            func_variables
            name=${prefix}${idx}-${capacity}-${sn}
            func_format
    	    ((idx++))
        done
    else
        exit 1
    fi
}

func_single() {
    echo "--> Entering single-disk mode."

    # Single device mode
    # Ask for device
    read -p "Which disk would you like to format? " -r disk

    # Ask for disk location
    read -p "Choose a prefix. This could be the physical location of $disk in the format you'd like to appear in it's label. \
    For example, to identify disk shelf 1, bay 11, you might enter s1d11:  " -r location

    func_variables
    name=${location}-${capacity}-${sn}

    read -p "If you confirm, $disk will be erased, partitioned, and given the label ${name}. \
    Should we continue? [y|n] " -n 1 -r
    echo ' '
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        func_format
    else
        exit 1
    fi
}



while getopts 'hsmi' OPTION; do
    case "$OPTION" in
        h)
	        func_help
	        exit 1
            ;;
        m)
	        func_multi
	        ;;
    	s)
    	    func_single
    	    ;;
        i)
            func_info
            ;;
    	?)
    	    func_help
    	    exit 1
    	    ;;
    esac
done


echo ' '
echo "These are the devices with proper labels"
if [[ $(uname) == FreeBSD ]]; then
    find /dev/gpt/ -type c
elif [[ $(uname) == Linux ]]; then
    find /dev/disk/by-partlabel/ -type c
fi
