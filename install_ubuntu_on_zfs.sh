#!/usr/bin/env bash

# Script for installing Ubuntu 22.04 on ZFS

ubuntu_version=jammy
HOSTNAME=
DISK=

setup_checks() {
    nc -vz ubuntu.com 443 || echo "Whoa, cowboy, you need internet access to run this script. Better get that sorted first!"
    
    # Disable automount
    gsettings set org.gnome.desktop.media-handling automount false
    
    
    echo "Running configuration checks ..."
    if [[ -z "$DISK" ]]; then
        echo "In order to run this script, you must first export the variable DISK to use the appropriate device from /dev/disk/by-id/"
        exit 1
    elif [[ -z "$HOSTNAME" ]]; then
        echo "You must export the target machine's hostname using the HOSTNAME variable to continue"
        exit 2
    fi
    
    apt update
    apt install --yes debootstrap gdisk zfsutils-linux zstd 
    systemctl stop zed
}

create_datasets() {
    # Disk prep
    echo "Preparing disk ..."
    swapoff --all
    wipefs -a $DISK
    
    # Do full TRIM/UNMAP on SSD
    if [[ $(cat /sys/block/sda/queue/rotational) -eq 0 ]]; then
        blkdiscard -f $DISK
    fi
    
    sgdisk --zap-all $DISK
    
    echo "Creating partitions ..."
    sgdisk     -n1:1M:+512M   -t1:EF00 $DISK    # EFI
    sgdisk     -n2:0:+500M    -t2:8200 $DISK    # Swap partition
    sgdisk     -n3:0:+2G      -t3:BE00 $DISK    # Boot pool
    sgdisk     -n4:0:0        -t4:BF00 $DISK    # Root pool
    
    echo "Creating boot pool ..."
    zpool create \
        -o ashift=12 \
        -o autotrim=on \
        -o cachefile=/etc/zfs/zpool.cache \
        -o compatibility=grub2 \
        -o feature@livelist=enabled \
        -o feature@zpool_checkpoint=enabled \
        -O devices=off \
        -O acltype=posixacl -O xattr=sa \
        -O compression=lz4 \
        -O normalization=formD \
        -O relatime=on \
        -O canmount=off -O mountpoint=/boot -R /mnt \
        bpool ${DISK}-part3
    
    echo "Creating encrypted root pool ..."
    zpool create \
        -o ashift=12 \
        -o autotrim=on \
        -O encryption=on -O keylocation=prompt -O keyformat=passphrase \
        -O acltype=posixacl -O xattr=sa -O dnodesize=auto \
        -O compression=zstd \
        -O normalization=formD \
        -O relatime=on \
        -O canmount=off -O mountpoint=/ -R /mnt \
        rpool ${DISK}-part4
    
    echo "Creating datasets ..."
    UUID=$(dd if=/dev/urandom bs=1 count=100 2>/dev/null |
        tr -dc 'a-z0-9' | cut -c-6)
    
    zfs create -o mountpoint=/ \
        -o com.ubuntu.zsys:bootfs=yes \
        -o com.ubuntu.zsys:last-used=$(date +%s) rpool/ROOT/ubuntu_$UUID
    
    zfs create -o mountpoint=/boot bpool/BOOT/ubuntu_$UUID
    
    zfs create -o canmount=off -o mountpoint=none rpool/ROOT
    zfs create -o canmount=off -o mountpoint=none bpool/BOOT
    zfs create -o com.ubuntu.zsys:bootfs=no -o canmount=off \
        rpool/ROOT/ubuntu_$UUID/usr
    zfs create -o com.ubuntu.zsys:bootfs=no -o canmount=off \
        rpool/ROOT/ubuntu_$UUID/var
    zfs create rpool/ROOT/ubuntu_$UUID/var/lib
    zfs create rpool/ROOT/ubuntu_$UUID/var/log
    zfs create rpool/ROOT/ubuntu_$UUID/var/spool
    
    zfs create -o canmount=off -o mountpoint=/ \
        rpool/USERDATA
    zfs create -o com.ubuntu.zsys:bootfs-datasets=rpool/ROOT/ubuntu_$UUID \
        -o canmount=on -o mountpoint=/root \
        rpool/USERDATA/root_$UUID
    chmod 700 /mnt/root
    
    # Optional datasets
    zfs create rpool/ROOT/ubuntu_$UUID/var/cache
    zfs create rpool/ROOT/ubuntu_$UUID/var/lib/nfs
    zfs create rpool/ROOT/ubuntu_$UUID/var/tmp
    chmod 1777 /mnt/var/tmp
    
    zfs create -o com.ubuntu.zsys:bootfs=no \
        rpool/ROOT/ubuntu_$UUID/srv
    
    zfs create rpool/ROOT/ubuntu_$UUID/var/lib/AccountsService
    zfs create rpool/ROOT/ubuntu_$UUID/var/lib/NetworkManager
    
    zfs create rpool/ROOT/ubuntu_$UUID/var/lib/docker
    
    zfs create rpool/ROOT/ubuntu_$UUID/var/snap
    
    mkdir /mnt/run
    mount -t tmpfs tmpfs /mnt/run
    mkdir /mnt/run/lock
    
    echo "Installing base system ..."
    debootstrap ${ubuntu_version} /mnt
    
    mkdir /mnt/etc/zfs
    cp /etc/zfs/zpool.cache /mnt/etc/zfs/
    
    echo "Configuring system ..."
    
    hostname "$HOSTNAME"
    hostname > /mnt/etc/hostname
    echo "127.0.1.1 $HOSTNAME" >> /mnt/etc/hosts
    
    echo 'deb http://archive.ubuntu.com/ubuntu jammy main restricted universe multiverse' > /mnt/etc/apt/sources.list
    echo 'deb http://archive.ubuntu.com/ubuntu jammy-updates main restricted universe multiverse' >> /mnt/etc/apt/sources.list
    echo 'deb http://archive.ubuntu.com/ubuntu jammy-backports main restricted universe multiverse' >> /mnt/etc/apt/sources.list
    echo 'deb http://security.ubuntu.com/ubuntu jammy-security main restricted universe multiverse' >> /mnt/etc/apt/sources.list
}

setup_chroot() {
    echo "It's time to chroot into the new system ..."
    mount --make-private --rbind /dev  /mnt/dev
    mount --make-private --rbind /proc /mnt/proc
    mount --make-private --rbind /sys  /mnt/sys
    chroot /mnt /usr/bin/env DISK=$DISK UUID=$UUID bash --login
    
    apt update
    
    dpkg-reconfigure locales tzdata keyboard-configuration console-setup
    apt install -y vim htop tmux curl wget git dosfstools
    
    mkdosfs -F 32 -s 1 -n EFI ${DISK}-part1
    mkdir /boot/efi
    echo /dev/disk/by-uuid/$(blkid -s UUID -o value ${DISK}-part1) \
        /boot/efi vfat defaults 0 0 >> /etc/fstab
    mount /boot/efi
    
    mkdir /boot/efi/grub /boot/grub
    echo /boot/efi/grub /boot/grub none defaults,bind 0 0 >> /etc/fstab
    mount /boot/grub
    
    apt install --yes \
        grub-efi-amd64 grub-efi-amd64-signed linux-image-generic \
        shim-signed zfs-initramfs
    
    apt purge --yes os-prober
    
    echo "Setting root password ..."
    passwd
    
    apt install --yes cryptsetup
    
    echo swap ${DISK}-part2 /dev/urandom \
          swap,cipher=aes-xts-plain64:sha256,size=512 >> /etc/crypttab
    echo /dev/mapper/swap none swap defaults 0 0 >> /etc/fstab
    
    cp /usr/share/systemd/tmp.mount /etc/systemd/system/
    systemctl enable tmp.mount
    
    
    addgroup --system lpadmin
    addgroup --system lxd
    addgroup --system sambashare
    
    apt install --yes openssh-server
    
    echo "Checking grub installation ..."
    grub-probe /boot
    
    update-initramfs -c -k all
    
    # Fix up grub file options
    sed -e '/GRUB_TIMEOUT_STYLE/ s/^#*/#/' -i /etc/default/grub # Comment out GRUB_TIMEOUT_STYLE
    sed -i 's/\(^GRUB_TIMEOUT=\).*/\15/' /etc/default/grub      # Replace GRUB_TIMEOUT value with 5
    sed -i '/GRUB_TIMEOUT/a \
    GRUB_RECORDFAIL_TIMEOUT=5' /etc/default/grub                # Insert GRUB_RECORDFAIL_TIMEOUT value below GRUB_TIMEOUT
    
    sed -i 's/\(^GRUB_CMDLINE_LINUX_DEFAULT=\).*/\1""/' /etc/default/grub   # Remove values from GRUB_CMDLINE_LINUX_DEFAULT
    sed -i 's/GRUB_TERMINAL=console/s/^#/' /etc/default/grub    # Uncomment GRUB_TERMINAL
    
    update-grub
    
    grub-install --target=x86_64-efi --efi-directory=/boot/efi \
        --bootloader-id=ubuntu --recheck --no-floppy
    
    mkdir /etc/zfs/zfs-list.cache
    touch /etc/zfs/zfs-list.cache/bpool
    touch /etc/zfs/zfs-list.cache/rpool
    zed -F &
    
    for i in bpool rpool; do
        echo "Catting out the $i cache"
        cat /etc/zfs/zfs-list.cache/$i
        read -p "Did you see any output from that command? If not, press N to fix it; otherwise press Y. Continue? (Y/N): " confirm 
        if [[ $confirm == [nN] ]]; then
            zfs set canmount=on "$i/BOOT/ubuntu_$UUID"
            cat /etc/zfs/zfs-list.cache/$i
        fi
    done
    
    kill $(pgrep zed)
    
    read -p "If you still didn't get any output from either of those items, you should cancel here and fix this manually. See https://openzfs.github.io/openzfs-docs for more info. Do you want to continue? (Y/N): " confirm_last_chance
    
    if [[ $confirm_last_chance == [nN] ]]; then
        echo "Exiting. You're still in the chroot. Manually start and stop zed and try again from this step."
        exit 3
    fi
    
    sed -Ei "s|/mnt/?|/|" /etc/zfs/zfs-list.cache/*
    
    echo "Exiting chroot ..."
    exit
}

# Do the stuff
# Become root
if [[ $UID -ne 0 ]]; then
    echo "Elevating privileges and preserving environment ..."
    for func in setup_checks create_datasets setup_chroot; do
        FUNC=$(declare -f $func)
        sudo -E bash -c "$FUNC; $func"
    done
fi

echo "Unmounting bind mounts"
mount | grep -v zfs | tac | awk '/\/mnt/ {print $3}' | \
    xargs -i{} umount -lf {}
zpool export -a

read -p "It's time to reboot so you can configure the user space. Visit https://openzfs.github.io/openzfs-docs for more info. Would you like to reboot? (Y/N): " confirm_reboot

if [[ $confirm_reboot == [yY] ]]; then
    shutdown -r now
else
    exit 0
fi
