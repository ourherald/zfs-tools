#!/usr/bin/env bash

printf '%s\n' "Welcome to the ZPOOL creator."

read -p "Enter the name you'd like to give the zpool we'll create. " -r pool_name

func_encrypt_choice() {
    while true; do
        read -p "Would you like to create $pool_name as an encrypted zpool? (y|N) " pool_encrypt
        case $pool_encrypt in
            [Yy]*)  encrypted=true ; return 0 ;;
            [Nn]*)  encrypted=false ; return 0 ;;
            #*)      echo "Didn't recognize your answer, bailing!"; exit 1 ;;
        esac
    done
}

func_user_choices() {
    func_encrypt_choice

    printf %s\n\n "These are available partitions for your pool:"

    if [[ $(uname) == Linux ]]; then
        find /dev/disk/by-partlabel/ -type l
    elif [[ $(uanme) == FreeBSD ]]; then
        find /dev/gpt/ -type c
    else
        echo "Couldn't find any pre-made partitions; you're on your own!"
    fi

    read -p "Enter the pool type and target partitions. For example, to create a striped mirror, you might enter `mirror /dev/sda2 /dev/sdb2 mirror /dev/sdc2 /dev/sdd2`. " pool_architecture

}

func_setup_encrypt() {
    secrets_dir=$HOME/.secrets
    if [[ $encrypted == true ]]; then
        mkdir -p $secrets_dir
        passphrase=$(openssl rand -hex 32)
        json_file=${secrets_dir}/${pool_name}_key.json
        hex_file=${secrets_dir}/${pool_name}_key.hex
        echo "{\"${pool_name}\": \"${passphrase}\" }" > $json_file
        echo "$passphrase" > $hex_file
    fi
}

func_pool_create() {
    printf "\n"
    printf "%s\n\n" "Copy the following command to create the $pool_name zpool:"
    printf "%s\n" "zpool create \\"
    printf "%s\n" "        -o ashift=12 \\"
    printf "%s\n" "        -o autotrim=on \\"
    printf "%s\n" "        -O encryption=on -O keyformat=hex -O keylocation=file://${hex_file} \\"
    printf "%s\n" "        -O acltype=posixacl -O xattr=sa -O dnodesize=auto \\"
    printf "%s\n" "        -O compression=zstd \\"
    printf "%s\n" "        -O normalization=formD \\"
    printf "%s\n" "        -O relatime=on \\"
    printf "%s\n" "        -O canmount=on -R /mnt \\"
    printf "%s\n" "        $pool_name ${pool_architecture}"
}

func_user_choices
func_setup_encrypt
func_pool_create
